module.exports = {
  plugins: [
    {
      resolve: 'gatsby-theme-baretheme',
      options: {
        plugins: [
          'gatsby-theme-baretheme-tinacms',
          'gatsby-theme-baretheme-text',
        ],
      },
    },
  ],
};
