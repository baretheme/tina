import { uniqBy } from 'lodash';

export const getBrowserPlugins = function getBrowserPlugins() {
  let barethemeBrowserPlugins = [];

  try {
    // eslint-disable-next-line
    barethemeBrowserPlugins= require('../../.cache/baretheme-browser-plugins').plugins;
  } catch (e) {
    barethemeBrowserPlugins = [];
  }

  return barethemeBrowserPlugins;
};

export const pluginBlocks = function pluginBlocks() {
  const plugins = getBrowserPlugins();
  return plugins.reduce((acc, plugin) => {
    if (!plugin.blocks || !Array.isArray(plugin.blocks)) return acc;
    const blocks = [...acc, ...plugin.blocks];
    return uniqBy(blocks, 'name');
  }, []);
};
