import React from 'react';
import PropTypes from 'prop-types';
import { pluginBlocks } from '../utils/plugins';

const blocks = pluginBlocks();

const Block = ({ block }) => {
  const current = blocks.find((b) => b.name === block.block);
  if (!current) {
    return (
      <div>Unknown block</div>
    );
  }
  const Component = current.component;
  return <Component {...block} />;
};

Block.propTypes = {
  block: PropTypes.shape({
    block: PropTypes.string,
  }).isRequired,
};

export default Block;
