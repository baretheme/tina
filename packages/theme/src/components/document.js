import React from 'react';
import PropTypes from 'prop-types';
import Layout from './layout';
import Block from './block';

const Document = ({ data }) => {
  const json = React.useMemo(() => JSON.parse(data.document.rawJson), [data.document.rawJson]);

  return (
    <Layout>
      { data.barethemeDocument.title }
      { json.blocks && json.blocks.map((block) => (
        <Block block={block} key={block.id} />
      ))}
    </Layout>
  );
};

Document.propTypes = {
  data: PropTypes.shape({
    document: PropTypes.shape({
      rawJson: PropTypes.string,
    }),
  }).isRequired,
};

export default Document;
