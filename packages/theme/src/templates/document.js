import React from 'react';
import { graphql } from 'gatsby';
import Document from '../components/document';

const BarethemeDocument = (props) => (
  <Document {...props} />
);

export default BarethemeDocument;

export const query = graphql`
  query BarethemeDocumentQuery($id: String!, $documentsJsonId: String!) {
    barethemeDocument(
      id: { eq: $id }
    ) {
      id
      date(formatString: "MMMM DD, YYYY")
      title
    }

    document: documentsJson(
      id: { eq: $documentsJsonId }
    ) {
      id
      date(formatString: "MMMM DD, YYYY")
      title
      rawJson
      fileRelativePath
    }

    theme: dataJson(
      fileRelativePath: { eq: "/content/data/theme.json" }
    ) {
      lightTheme {
        accent
        background
        dimmed
        error
        faded
        foreground
        highlight
        lowered
        overlay
        raised
        shadow
        success
      }

      darkTheme {
        accent
        background
        dimmed
        error
        faded
        foreground
        highlight
        lowered
        overlay
        raised
        shadow
        success
      }
      rawJson
      fileRelativePath
    }

    site: dataJson(
      fileRelativePath: { eq: "/content/data/site.json" }
    ) {
      title
      description
      defaultLanguage
      logo {
        publicURL
      }
      languages {
        code
        title
      }
      rawJson
      fileRelativePath
    }

    navigation: dataJson(
      fileRelativePath: { eq: "/content/data/navigation.json" }
    ) {
      main {
        label
        link
      }
      rawJson
      fileRelativePath
    }
  }
`;
