const { omit } = require('lodash');
const onPreBootstrap = require('./gatsby/on-pre-bootstrap');
const createPages = require('./gatsby/create-pages');

exports.onCreateNode = ({
  node, actions, getNode, getNodes,
}, options) => {
  if (node.internal.type === 'BarethemeDocument') {
    const { createNodeField } = actions;
    const nodes = getNodes();
    const jsonNode = getNode(node.parent);
    const fileNode = getNode(jsonNode.parent);
    const defaultLanguage = options.defaultLanguage || 'en';
    let path = fileNode.name === 'index' ? '/' : node.slug;

    if (defaultLanguage !== node.language) {
      path = `${node.language}/${node.slug}`;
    }

    if (fileNode && fileNode.relativeDirectory) {
      const directoryFileNode = nodes.find((node) => node.base === `${fileNode.relativeDirectory}.json`);
      const directoryJsonNode = getNode(directoryFileNode.children[0]);
      const directoryVersion = directoryJsonNode.versions.find((version) => version.language === node.language);
      path = `${directoryVersion.slug}/${path}`;
    }

    if (path !== '/') {
      path = path.replace(/([^:]\/)\/+/g, '$1'); // remove double slashes
    }

    createNodeField({
      node,
      name: 'path',
      value: path,
    });
  }
};

exports.sourceNodes = async ({
  actions,
  getNodes,
  createNodeId,
  createContentDigest,
}, options) => {
  const { createNode, createParentChildLink } = actions;
  const nodes = getNodes();
  const documentNodes = nodes.filter((node) => node.internal.type === 'documentsJson');

  documentNodes.forEach((node) => {
    node.versions.forEach((version) => {
      const data = omit(node, ['internal', 'versions', 'children', 'parent']);
      const useDrafts = options.drafts || true;
      const isDraft = node.draft && version.draft;

      const newNode = {
        ...data,
        ...version,
        parent: node.id,
        id: createNodeId(`${node.id} >>> BarethemeDocument`),
        published: useDrafts ? true : !isDraft,
        internal: {
          type: 'BarethemeDocument',
          contentDigest: createContentDigest(node),
        },
      };
      createParentChildLink({ parent: node, child: newNode });
      createNode(newNode);
    });
  });
};

exports.createPages = createPages;
exports.onPreBootstrap = onPreBootstrap;
