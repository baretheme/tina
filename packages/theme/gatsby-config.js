module.exports = (themeOptions) => {
  // define some nice defaults that can be overwritten in the theme options
  const defaults = {
    defaultTheme: 'dark',
    plugins: [],
  };

  // let's merge our defaults together with the themeOptions
  const options = { ...defaults, ...themeOptions };
  const { plugins } = options;

  return {
    plugins: [
      ...plugins,
      'gatsby-transformer-sharp',
      'gatsby-plugin-sharp',
      'gatsby-plugin-offline',
      'gatsby-tinacms-json',
      {
        resolve: 'gatsby-transformer-json',
        options: {
          typeName: ({ node }) => `${node.sourceInstanceName}Json`,
        },
      },
      {
        resolve: 'gatsby-source-filesystem',
        options: {
          path: 'content/documents',
          name: 'documents',
        },
      },
      {
        resolve: 'gatsby-source-filesystem',
        options: {
          path: 'content/assets',
          name: 'assets',
        },
      },
      {
        resolve: 'gatsby-source-filesystem',
        options: {
          path: 'content/data',
          name: 'data',
        },
      },
      {
        resolve: 'gatsby-transformer-remark',
        options: {
          plugins: [
            'gatsby-remark-prismjs',
            'gatsby-remark-smartypants',
            {
              resolve: 'gatsby-remark-images',
              options: {
                maxWidth: 880,
                withWebp: true,
              },
            },
            {
              resolve: 'gatsby-remark-responsive-iframe',
              options: {
                wrapperStyle: 'margin-bottom: 1.0725rem',
              },
            },
            {
              resolve: 'gatsby-remark-copy-linked-files',
              options: {
                destinationDir: 'static',
              },
            },
          ],
        },
      },
    ],
  };
};
