const path = require('path');
const fse = require('fs-extra');
const { camelCase } = require('lodash');
const dedent = require('dedent-js');
const Terser = require('terser');

// We need some place to save the files
const cacheDir = path.resolve(__dirname, '../.cache');

// Iterates through all plugins and saves them to a new file so we can import them on client side
function getBarethemePlugins(plugins) {
  const paths = [];

  plugins.forEach((plugin) => {
    paths.push(`${plugin.name}/baretheme-browser`);
  });

  const browserPlugins = plugins.map((plugin) => ({
    name: camelCase(plugin.name),
    path: `${plugin.name}/baretheme-browser`,
  }));

  const imports = browserPlugins.reduce((acc, plugin) => `
    ${acc}
    try {
      const plugin = require("${plugin.path}");
      if (plugin) {
        plugins.push({
          name: "${plugin.name}",
          ...plugin,
        });
      }
    } catch {
      // ignore
    }
  `, '');

  const file = dedent`
    const plugins = []
    ${imports}
    export { plugins };
  `;

  const minified = Terser.minify(file);
  fse.outputFile(path.resolve(cacheDir, 'baretheme-browser-plugins.js'), minified.code);
}

module.exports = async (ctx, themeOptions) => {
  fse.ensureDirSync(cacheDir);
  fse.emptyDirSync(cacheDir);
  getBarethemePlugins(themeOptions.plugins);
};
