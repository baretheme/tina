const path = require('path');

module.exports = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions;
  const documentTemplate = path.resolve(__dirname, '../src/templates/document.js');
  const result = await graphql(
    `
      query BarethemeDocumentQuery {
        documents: allBarethemeDocument {
          edges {
            node {
              id
              published
              fields {
                path
              }
              parent {
                ... on documentsJson {
                  id
                }
              }
            }
          }
        }
      }
    `,
  );

  if (result.errors) {
    reporter.panicOnBuild('[BARETHEME] Error while running GraphQL query.');
    throw result.errors;
  }

  const documents = result.data.documents.edges;

  documents.forEach((edge) => {
    const { node } = edge;
    const { published, fields, parent } = node;
    const { path } = fields;

    if (!published) return;

    const page = {
      path,
      component: documentTemplate,
      context: {
        id: node.id,
        documentsJsonId: parent.id,
      },
    };

    createPage(page);
  });
};
