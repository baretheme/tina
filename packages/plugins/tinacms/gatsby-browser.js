import React from 'react';
import PropTypes from 'prop-types';
import TinaCMSConditionField from 'tinacms-condition-field';
import TinaCMSRelationField from 'tinacms-relation-field';
import TinaCMSFileField from 'tinacms-file-field';
import useLanguages from './src/hooks/use-languages';
import useCollections from './src/hooks/use-collections';
import useDocuments from './src/hooks/use-documents';
import TinaCMSDocument from './src/components/tinacms-document';
import Versions from './src/fields/versions';

export const wrapPageElement = ({ element }) => (
  <TinaCMSDocument data={element.props.data}>
    {(data) => (
      React.cloneElement(element, {
        ...element.props,
        data,
      })
    )}
  </TinaCMSDocument>
);

wrapPageElement.propTypes = {
  element: PropTypes.node.isRequired,
};

const LanguageVersions = ({ field, ...props }) => {
  const languages = useLanguages();
  const newField = {
    ...field,
    data: languages,
  };
  return (
    <Versions
      field={newField}
      {...props}
    />
  );
};

LanguageVersions.propTypes = {
  field: PropTypes.shape({}).isRequired,
};

export const onClientEntry = () => {
  window.tinacms.fields.add({
    name: 'language-versions',
    Component: LanguageVersions,
  });

  const conditionField = new TinaCMSConditionField(window.tinacms);
  conditionField.install();

  const fileField = new TinaCMSFileField(window.tinacms);
  fileField.install();

  const relationField = new TinaCMSRelationField(window.tinacms);

  relationField.install([{
    name: 'language',
    hook: useLanguages,
    itemProps: (item) => ({
      key: item.code,
      label: item.title,
    }),
    noDataText: "You didn't create any language.",
    sortable: false,
    multiple: false,
  }]);

  relationField.install([{
    name: 'collections',
    hook: useCollections,
    itemProps: (item) => ({
      key: item.id,
      label: item.title,
    }),
    noDataText: "You didn't create any collection.",
    sortable: true,
    multiple: true,
  }]);

  relationField.install([{
    name: 'document',
    hook: useDocuments,
    itemProps: (item) => ({
      key: item.id,
      label: item.title,
    }),
    noDataText: "You didn't create any document.",
    sortable: false,
    multiple: false,
  }]);
};
