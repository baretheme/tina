// import React from 'react';
import PropTypes from 'prop-types';
import { useLocalJsonForm, JsonCreatorPlugin, useGlobalJsonForm } from 'gatsby-tinacms-json';
import { withPlugin } from 'tinacms';
import { editSiteForm } from '../forms/site';
import { editNavigationForm } from '../forms/navigation';
import { createDocumentForm, editDocumentForm } from '../forms/document';
import { editThemeForm } from '../forms/theme';

const TinaCMSDocument = ({ data, children }) => {
  const [document] = useLocalJsonForm(data.document, editDocumentForm(data));
  const [site] = useGlobalJsonForm(data.site, editSiteForm);
  const [navigation] = useGlobalJsonForm(data.navigation, editNavigationForm);
  const [theme] = useGlobalJsonForm(data.theme, editThemeForm);

  const newData = {
    ...data,
    document,
    site,
    navigation,
    theme,
    // languages,
  };

  return children(newData);
};

TinaCMSDocument.propTypes = {
  data: PropTypes.shape({
    document: PropTypes.object,
    site: PropTypes.object,
    navigation: PropTypes.object,
    theme: PropTypes.object,
  }).isRequired,
};

const CreateDocumentPlugin = new JsonCreatorPlugin(createDocumentForm);

export default withPlugin(TinaCMSDocument, CreateDocumentPlugin);
