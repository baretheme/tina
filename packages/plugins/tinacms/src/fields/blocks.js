import { v4 as uuidv4 } from 'uuid';
import { pluginBlocks } from 'gatsby-theme-baretheme';

const rawBlocks = pluginBlocks();

const marginOptions = ['None', 'Small', 'Medium', 'Large'];
export const margins = [
  {
    name: 'spacings',
    component: 'group',
    label: 'Spacings',
    fields: [
      {
        name: 'marginTop',
        component: 'select',
        label: 'Margin Top',
        options: marginOptions,
      },
      {
        name: 'marginBottom',
        component: 'select',
        label: 'Margin Bottom',
        options: marginOptions,
      },
    ],
  },
];

const blocks = rawBlocks.map((block) => ({
  ...block,
  key: block.name,
  defaultItem: (item) => ({
    ...block.defaultItem(item),
    id: uuidv4(),
    block: block.name,
  }),
  fields: [
    ...block.fields,
    ...margins,
  ],
}));

export { blocks };
