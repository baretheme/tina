import { useStaticQuery, graphql } from 'gatsby';

export default () => {
  const { site } = useStaticQuery(
    graphql`
      query allCollectionsQuery {
        site: dataJson(
          fileRelativePath: { eq: "/content/data/site.json" }
        ) {
          collections {
            id
            title
          }
        }
      }
    `,
  );

  return site.collections;
};
