import { useStaticQuery, graphql } from 'gatsby';

export default () => {
  const { site } = useStaticQuery(
    graphql`
      query allLanguagesQuery {
        site: dataJson(
          fileRelativePath: { eq: "/content/data/site.json" }
        ) {
          languages {
            code
            title
          }
        }
      }
    `,
  );

  return site.languages;
};
