import { useStaticQuery, graphql } from "gatsby"

export default () => {
  const { documents } = useStaticQuery(
    graphql`
      query allDocumentQuery {
        documents: allDocumentsJson {
          nodes {
            id
            title
          }
        }
      }
    `
  )

  return documents.nodes
}