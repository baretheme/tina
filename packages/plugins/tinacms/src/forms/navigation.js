export const editNavigationForm = {
  label: 'Navigation',
  fields: [
    {
      label: 'Main Menu',
      name: 'rawJson.main',
      component: 'group-list',
      itemProps: (item) => ({
        label: item.label,
      }),
      defaultItem: () => ({
        label: 'New Menu Item',
        internal: true,
      }),
      fields: [
        {
          label: 'Label',
          name: 'label',
          component: 'text',
          parse(value) {
            return value || '';
          },
        },
        {
          label: 'Internal',
          name: 'internal',
          description: 'Set to false if you want link to another website',
          component: 'condition',
          trigger: {
            component: 'toggle',
          },
          fields: (value) => (value ? [
            {
              label: 'Document',
              name: 'document',
              component: 'document',
              parse(value) {
                return value || '';
              },
            },
          ] : [
            {
              label: 'URL',
              name: 'url',
              component: 'text',
            },
          ]),
        },
      ],
    },
  ],
};
