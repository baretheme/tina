const colors = [
  {
    name: 'accent',
    component: 'color',
    label: 'Accent',
    colorFormat: 'hex',
  },
  {
    name: 'foreground',
    component: 'color',
    label: 'Foreground',
    colorFormat: 'hex',
  },
  {
    name: 'background',
    component: 'color',
    label: 'Background',
    colorFormat: 'hex',
  },
  {
    name: 'dimmed',
    component: 'color',
    label: 'Dimmed',
    colorFormat: 'hex',
  },
  {
    name: 'faded',
    component: 'color',
    label: 'Faded',
    colorFormat: 'hex',
  },
  {
    name: 'highlight',
    component: 'color',
    label: 'Highlight',
    colorFormat: 'hex',
  },
  {
    name: 'raised',
    component: 'color',
    label: 'Raised',
    colorFormat: 'hex',
  },
  {
    name: 'lowered',
    component: 'color',
    label: 'Lowered',
    colorFormat: 'hex',
  },
  {
    name: 'shadow',
    component: 'color',
    label: 'Shadow',
    colorFormat: 'hex',
  },
  {
    name: 'overlay',
    component: 'color',
    label: 'Overlay',
    colorFormat: 'hex',
  },
  {
    name: 'error',
    component: 'color',
    label: 'Error',
    colorFormat: 'hex',
  },
];

export const editThemeForm = {
  label: 'Theme',
  fields: [
    {
      name: 'rawJson.lightTheme',
      component: 'group',
      label: 'Light Theme',
      fields: colors,
    },
    {
      name: 'rawJson.darkTheme',
      component: 'group',
      label: 'Dark Theme',
      fields: colors,
    },
  ],
};
