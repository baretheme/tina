import slugify from 'slugify';
import { DeleteAction } from 'gatsby-tinacms-remark';
import { blocks } from '../fields/blocks';

export const createDocumentForm = {
  label: 'New Document',
  fields: [
    { name: 'title', component: 'text', label: 'Title' },
  ],
  filename(form) {
    const slug = slugify(form.title.toLowerCase());
    return `content/documents/${form.language}/${slug}.json`;
  },
  data(form) {
    return {
      createdAt: new Date(),
      title: form.title,
    };
  },
};

export const editDocumentForm = (data) => ({
  actions: [DeleteAction],
  label: `${data.document.title}`,
  fields: [
    {
      name: 'rawJson.title',
      component: 'text',
      label: 'Title',
    },
    {
      name: 'rawJson.draft',
      component: 'toggle',
      label: 'Draft',
    },
    {
      name: 'collections',
      component: 'collections',
      label: 'Collections',
    },
    {
      name: 'rawJson.versions',
      component: 'language-versions',
      label: 'Versions',
      itemProps: (item) => {
        console.log(item);
        return {
          key: item.language,
          label: item.draft ? `${item.title} (draft)` : item.title,
        };
      },
      fields: [
        {
          label: 'Title',
          name: 'title',
          component: 'text',
        },
        {
          name: 'draft',
          component: 'toggle',
          label: 'Draft',
        },
        {
          label: 'Slug',
          name: 'slug',
          component: 'text',
        },
        {
          name: 'language',
          component: 'language',
          label: 'Language',
          disabled: true,
        },
        {
          name: 'meta',
          component: 'group',
          label: 'Meta',
          fields: [
            {
              name: 'title',
              component: 'text',
              label: 'Title',
            },
            {
              name: 'description',
              component: 'text',
              label: 'Description',
            },
          ],
        },
        {
          label: 'Blocks',
          name: 'blocks',
          component: 'blocks',
          templates: blocks,
        },
      ],
    },
  ],
});
