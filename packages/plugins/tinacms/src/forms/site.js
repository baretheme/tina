import { v4 as uuidv4 } from 'uuid';

export const editSiteForm = {
  label: 'Site',
  fields: [
    {
      label: 'Title',
      name: 'rawJson.title',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'Description',
      name: 'rawJson.description',
      component: 'text',
      parse(value) {
        return value || '';
      },
    },
    {
      label: 'Logo',
      name: 'rawJson.logo',
      component: 'file',
      description: 'Please provide an svg logo',
      accept: 'image/svg+xml',
      uploadDir: () => '/content/assets/',
      parse: (filename) => `../assets/${filename}`,
    },
    {
      label: 'Default language',
      name: 'rawJson.defaultLanguage',
      component: 'language',
    },
    {
      label: 'Languages',
      name: 'rawJson.languages',
      component: 'group-list',
      itemProps: (item) => ({
        key: item.id,
        label: item.title,
      }),
      defaultItem: () => ({
        id: uuidv4(),
        code: '',
        title: 'New Language',
      }),
      fields: [
        {
          label: 'Title',
          name: 'title',
          component: 'text',
        },
        {
          label: 'Code',
          name: 'code',
          component: 'text',
        },
      ],
    },
    {
      label: 'Collections',
      name: 'rawJson.collections',
      component: 'group-list',
      itemProps: (item) => ({
        key: item.id,
        label: item.title,
      }),
      defaultItem: () => ({
        id: uuidv4(),
        title: 'New Collection',
      }),
      fields: [
        {
          label: 'Title',
          name: 'title',
          component: 'text',
        },
      ],
    },
  ],
};
