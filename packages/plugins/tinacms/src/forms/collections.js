import slugify from "slugify"
import { DeleteAction } from "gatsby-tinacms-remark"

export const createCollectionForm = {
  label: "New Collection",
  fields: [
    { name: "title", component: "text", label: "Title" },
  ],
  filename(form) {
    const slug = slugify(form.title.toLowerCase())
    return `content/collections/${slug}.json`
  },
  data(form) {
    const slug = slugify(form.title.toLowerCase())
    return {
      createdAt: new Date(),
      updatedAt: null,
      title: form.title,
      slug,
    }
  },
}

export const editDocumentForm = {
  actions: [DeleteAction],
  fields: [
    {
      label: "Title",
      name: "rawJson.title",
      component: "text",
    },
  ],
}