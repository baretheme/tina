module.exports = {
  plugins: [
    {
      resolve: 'gatsby-plugin-tinacms',
      options: {
        sidebar: {
          hidden: process.env.NODE_ENV === 'production',
          position: 'displace',
          // theme: {
          //   color: {
          //     primary: {
          //       light: theme.color.primary,
          //       medium: theme.color.primary,
          //       dark: theme.color.primary,
          //     },
          //   },
          // },
        },
        plugins: [
          'gatsby-tinacms-git',
          'gatsby-tinacms-remark',
        ],
      },
    },
  ],
};
