import React from 'react';
import PropTypes from 'prop-types';

const TextBlock = ({ content }) => (
  <div>
    { content && (
      <div dangerouslySetInnerHTML={{ __html: content }} />
    )}
  </div>
);

TextBlock.defaultProps = {
  content: undefined,
};

TextBlock.propTypes = {
  content: PropTypes.string,
};

export default TextBlock;
