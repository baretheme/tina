import TextBlock from './src/components/text-block';

export const blocks = [
  {
    name: 'text',
    label: 'Text',
    component: TextBlock,
    itemProps: (item) => ({
      key: item.id,
      label: item.title,
    }),
    defaultItem: () => ({
      title: 'New text block',
      content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
    }),
    fields: [
      { name: 'title', label: 'Title', component: 'text' },
      { name: 'content', label: 'Content', component: 'html' },
    ],
  },
];
